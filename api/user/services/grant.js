'use strict';

/**
 * Module dependencies.
 */

// Public node modules.
const _ = require('lodash');

// Purest strategies.
const Purest = require('purest');

const facebook = new Purest({
  provider: 'facebook'
});

const vk = new Purest({
  provider: 'vk'
});

const github = new Purest({
  provider: 'github',
  defaults: {
    headers: {
      'user-agent': 'strapi'
    }
  }
});

const google = new Purest({
  provider: 'google'
});

const linkedin = new Purest({
  provider: 'linkedin'
});

/**
 * Connect thanks to a third-party provider.
 *
 *
 * @param {String}    provider
 * @param {String}    access_token
 * @param {Object}    profile
 *
 * @return  {*}
 */

exports.connect = function * connect(provider, access_token, profile) {
  return new Promise(function (resolve, reject) {
    if (!access_token) {
      reject({
        message: 'No access_token.'
      });
    } else {
      if (profile && profile.email) {
        if (provider !== 'local') {
          Social.findOne({ email: profile.email }).exec(function (err, social) {
            if (err) reject(err);
            else if (!social) {
              const params = {
                provider,
                accessToken: access_token,
                email: profile.email
              };
              console.log('params',params);
              Social.create(params).exec(function (err, social) {
                console.log('social',social);
                console.log('err',err);
                if (err) reject(err);
                else {
                  User.findOne({ email: profile.email }).exec(function (err, user) {
                    if (err) {
                      reject(err);
                    } else if (!user) {
                      console.log('social if no user',social);
                      // Create the new user.
                      const params = _.assign(profile, { username: profile.email }, {
                        id_ref: 1,
                        lang: strapi.config.i18n.defaultLocale,
                        template: 'standard',
                        provider: provider,
                        socials: [social]
                      });

                      User.create(params).exec(function (err, user) {
                        if (err) {
                          reject(err);
                        } else {
                          social.createdBy = user;
                          social.save();
                          resolve(user);
                        }
                      });
                    } else {
                      resolve(user);
                    }
                  });
                }
              });
            }
          });
        }
      } else {
        // Get the profile.
        getProfile(provider, access_token, function (err, profile) {
          if (err) {
            reject(err);
          } else {
            // We need at least the mail.
            if (!profile.email) {
              reject({
                message: 'Email was not available.'
              });
            } else {
              User.findOne({ email: profile.email }).exec(function (err, user) {
                if (err) {
                  reject(err);
                } else if (!user) {
                  // Create the new user.
                  const params = _.assign(profile, {
                    id_ref: 1,
                    lang: strapi.config.i18n.defaultLocale,
                    template: 'standard',
                    provider: provider
                  });

                  User.create(params).exec(function (err, user) {
                    if (err) {
                      reject(err);
                    } else {
                      resolve(user);
                    }
                  });
                } else {
                  resolve(user);
                }
              });
            }
          }
        });
      }
    }
  });
};

/**
 * Connect thanks to a third-party provider.
 *
 *
 * @param {String}    provider
 * @param {String}    access_token
 * @param {Object}    user
 *
 * @return  {*}
 */

exports.connectToUser = function * connectToUser(provider, access_token, user) {
  return new Promise(function(resolve, reject) {
    if (!access_token) return reject({ message: 'No access_token.' });
    const params = {
      provider,
      access_token,
      email: user.email,
      createdBy: user
    };
    Social.create(params, function (err, social) {
      if (err) reject(err);
      else {
        User.findOne({ email: user.email }, function (err, user) {
          if (err) reject(err);
          else {
            const updatedSocials = user.socials.slice();
            updatedSocials.push(social);
            user.socials = updatedSocials;
            user.save();
            resolve(user);
          }
        });
      }
    });
  });
};

/**
 * Helper to get profiles
 *
 * @param {String}   provider
 * @param {String}   access_token
 * @param {Function} callback
 */

function getProfile(provider, access_token, callback) {
  let fields;
  switch (provider) {
    case 'facebook':
      facebook.query().get('me?fields=name,email').auth(access_token).request(function (err, res, body) {
        if (err) {
          callback(err);
        } else {
          callback(null, {
            username: body.name,
            email: body.email
          });
        }
      });
      break;
    case 'vk':
      vk.query().get('account.getInfo').auth(access_token).request(function (err, res, body) {
        if (err) {
          callback(err);
        } else {
          callback(null, {
            username: body.email,
            email: body.email
          });
        }
      });
      break;
    case 'google':
      google.query('plus').get('people/me').auth(access_token).request(function (err, res, body) {
        if (err) {
          callback(err);
        } else {
          callback(null, {
            username: body.displayName,
            email: body.emails[0].value
          });
        }
      });
      break;
    case 'github':
      github.query().get('user').auth(access_token).request(function (err, res, body) {
        if (err) {
          callback(err);
        } else {
          callback(null, {
            username: body.login,
            email: body.email
          });
        }
      });
      break;
    case 'linkedin2':
      fields = [
        'public-profile-url', 'email-address'
      ];
      linkedin.query().select('people/~:(' + fields.join() + ')?format=json').auth(access_token).request(function (err, res, body) {
        if (err) {
          callback(err);
        } else {
          callback(null, {
            username: substr(body.publicProfileUrl.lastIndexOf('/') + 1),
            email: body.emailAddress
          });
        }
      });
      break;
    default:
      callback({
        message: 'Unknown provider.'
      });
      break;
  }
}
