const fetch = require('node-fetch');
const jsonfile = require('jsonfile');
const path = require('path');

const url = 'https://www.sofascore.com/tennis/livescore/json';
const filePath = path.join(__dirname, 'public', 'data.json');
const fileEventsPath = path.join(__dirname, 'public', 'dataEvents.json');

function fetchAllLiveEvents() {
  fetch(url)
  .then(res => res.json())
  .then(async json => {
    let tournaments = (json.sportItem || {}).tournaments || [];
    tournaments = tournaments.map(t => {
      t.events = (t.events || []).filter(event => 'point' in event.homeScore && 'point' in event.awayScore);
      t.eventIds = t.events.map(e => e.id);
      delete t.events;
      return t;
    });
    tournaments = tournaments.filter(t => t.eventIds.length);
    jsonfile.writeFileSync(filePath, tournaments);
    console.log('Fetched! Date:', new Date().toString());
    return json;
  })
  .catch(error => {
    console.log('error', error);
  });
}

fetchAllLiveEvents();

const interval = setInterval(fetchAllLiveEvents, 30000);

module.exports.filePath = filePath;
module.exports.fileEventsPath = fileEventsPath;
