const fetch = require('node-fetch');
const jsonfile = require('jsonfile');
const path = require('path');

const filePath = require('../fetcher.js').filePath;
const fileEventsPath = require('../fetcher.js').fileEventsPath;

const eventUrl = id => `https://www.sofascore.com/event/${id}/general/json`;

function parseEvent(eventId) {
  return fetch(eventUrl(eventId))
  .then(data => data.json())
  .then(json => json)
  .catch(() => {});
}

const womenReg = /women|woman/ig;

async function fetchEvents() {
  const tournaments = jsonfile.readFileSync(filePath).filter(t => t.season && womenReg.test(t.season.slug));
  const eventIds = tournaments.reduce((prev, cur) => [ ...prev, ...cur.eventIds ], []);
  //console.log('eventIds',eventIds);
  let events = eventIds.map(parseEvent);
  events = await Promise.all(events);
  //console.log('events',events);
  events = events.map(e => {
    delete e.odds;
    //delete e.statisticsItems;
    //delete e.periods;
    //delete e.highlights;
    //delete e.statistics;
    return e;
  });
  //events = events.filter(e => e.pointByPoint[0] && e.pointByPoint[0].games && e.pointByPoint[0].games.length === 11);
  jsonfile.writeFileSync(fileEventsPath, events);
}

fetchEvents();

const intervalEvents = setInterval(fetchEvents, 5000);
